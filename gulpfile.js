/**
 * Generic gulp file for different kind of projet
 * Thomas CILES
**/
"use strict";

// MODULES
var gulp =  require('gulp');
//var gulpif = require('gulpif');
var bs = require('browser-sync').create();
var cache = require('gulp-cache');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minify = require('minify');
var cleanCSS = require('gulp-clean-css');
var gutil = require('gulp-util');
var clean = require('gulp-clean');
var plumber = require('gulp-plumber');
var imagemin = require('gulp-imagemin');
var iconfont = require('gulp-iconfont');
var iconfontCss = require('gulp-iconfont-css');
var ftp = require('gulp-ftp');


var runTimestamp = Math.round(Date.now()/1000);
var PROJECT_NAME = 'gulp';

var FTP_CONFIG = {
    host: '',
    user: '',
	pass: '',
	port: 21
};


// WORDPRESS
var WP = false;
var PROJECT = null;


// PATHS
var CSS_PATH = 'css';
var SCSS_PATH = 'sass';
var JS_PATH = 'js';
var IMG_PATH = 'img';
var HTML_PATH = '.';
var ALL_CSS = 'style.min.css';
var FONT_ASSETS_PATH = 'fonts/assets';
var FONT_PATH = 'fonts';
var FONT_NAME = 'test';

// BROWSERS
var browsersOpt = '> 1%, last 3 versions, Firefox ESR';


// If wordpress update theme root folder
if(WP == true) {
	CSS_PATH = '/wp-content/themes/' + PROJECT;
	HTML_PATH = '/wp-content/themes/' + PROJECT;
	JS_PATH = '/wp-content/themes/js' + PROJECT;
	IMG_PATH = '/wp-content/themes/img' + PROJECT;
	SCSS_PATH = '/wp-content/themes/sass' + PROJECT;
	FONT_PATH = '/wp-content/themes/fonts' + PROJECT;
	FONT_ASSETS_PATH = '/wp-content/themes/fonts/assets' + PROJECT;
}

gulp.task('uploadStyle', function () {

	if(!FTP_CONFIG.user && !FTP_CONFIG.host && !FTP_CONFIG.pass) {
		gutil.log('No FTP available configuration.', 'Really it did', gutil.colors.magenta('FTP'));
		return false;
	}

    return gulp.src(CSS_PATH + '/' + ALL_CSS)
        .pipe(ftp(FTP_CONFIG))
        .pipe(gutil.noop());
});

gulp.task('css', function() {
	
	return gulp
		.src([CSS_PATH + '/**/*.css', '!' + CSS_PATH + '/**/' + ALL_CSS])
		.pipe(autoprefixer({
            browsers: browsersOpt,
            cascade: false
        }))
        .pipe(concat(ALL_CSS))
        .pipe(cleanCSS({debug: true}, function(details) {
            console.log(details.name + ': ' + details.stats.originalSize);
            console.log(details.name + ': ' + details.stats.minifiedSize);
        }))
		.pipe(gulp.dest(CSS_PATH))
		.pipe(bs.stream())
		.on('error', gutil.log);

});


gulp.task('sass', function() {

	return gulp
		.src(SCSS_PATH + '/**/*.scss')
		.pipe(plumber())
		.pipe(sass().on('error', sass.logError))
		//.pipe(plumber.stop())
		.pipe(gulp.dest(CSS_PATH))
		.on('error', gutil.log);

});


gulp.task('html', function() {

	return gulp
		.src([HTML_PATH + '/**/*.html', HTML_PATH + '/**/*.php'])
		.pipe(gulp.dest(HTML_PATH))
		.pipe(bs.reload())
		.on('error', gutil.log);

});


gulp.task('js', function() {

	return gulp
		.src([JS_PATH + '/**/*.js', JS_PATH + '/**/*.vue'])
		.pipe(minify())
		.pipe(gulp.dest(JS_PATH))
		.pipe(bs.reload())
		.on('error', gutil.log);

});


gulp.task('serve', function() {
	bs.init({
        server: {
        	baseDir : './',
            port : 3000
        },
        open : false,
        //proxy : 'http://localhost'
    });

});


gulp.task('clean', function () {  
  return gulp.src(CSS_PATH, {read: false})
    .pipe(clean());
});

gulp.task('img', function() {
	return gulp
		.src(IMG_PATH + '**/*.{png,jpg,jpeg,gif,svg}')
		.pipe(imagemin());
});


gulp.task('iconfont', function(){
  return gulp
  	.src([FONT_ASSETS_PATH + '**/*.svg'])
  	.pipe(iconfontCss({
      fontName: FONT_NAME,
      path: CSS_PATH,
      targetPath: '../' + SCSS_PATH + '/fonts/_' + FONT_NAME + '.scss',
      fontPath: FONT_PATH + '/'
    }))
    .pipe(iconfont({
      fontName: FONT_NAME, // required
      prependUnicode: true, // recommended option
      formats: ['ttf', 'eot', 'woff', 'woff2'],
      timestamp: runTimestamp,
    }))
      .on('glyphs', function(glyphs, options) {
        // CSS templating, e.g.
        console.log(glyphs, options);
      })
    .pipe(gulp.dest(FONT_PATH));
});


gulp.task('default', ['serve', 'img', 'iconfont'], function() {

	gutil.log(gutil.colors.red('Hey ! I\'m the v1 of gulp'));

    gulp.watch(SCSS_PATH + '/**/*.scss', ['sass']);
  	gulp.watch(CSS_PATH + '/**/*.css', ['css', 'uploadStyle']);
  	gulp.watch(JS_PATH + '/**/*.{js,ts,vue}', ['js']);
  	gulp.watch(HTML_PATH + '/**/*.{html,php}', ['html']);
  	gulp.watch(IMG_PATH + '/**/*.{png,jpg,jpeg,gif,svg}', ['img']);

});



